#!/bin/bash

USAGE="$0 <install|run>"
HERE=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
. ${HERE}/functions.sh

INSTALL_OR_RUN=$1
if [ -z $INSTALL_OR_RUN ]; then
  echo $USAGE
  exit 1
fi

if [ $INSTALL_OR_RUN == "run" ]; then
  HERE=$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)
  cd $HERE

  echo "Starting garage.py"
  FLASK_APP=garage.py flask run -h 0.0.0.0
elif [ $INSTALL_OR_RUN == "install" ]; then
  echo "Installing..."
  install $HERE
  echo "Finished installing."
fi
