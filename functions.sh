#!/bin/bash

function get_architecture() {
  echo $(lscpu | grep -o 'armv[[:digit:]]')
}

function install_prometheus() {
  version=${PROM_VERSION:-2.33.4}
  architecture=$(get_architecture)
  unpack_dir=prometheus-${version}.linux-${architecture}
  etc_dir=${ETC_DIR:-/etc/prometheus}
  bin_dir=${BIN_DIR:-/usr/local/bin}
  prom_data_dir=${PROMETHEUS_DATA_DIR:-/var/lib/prometheus/data}

  if ! [ -d ${unpack_dir} ]; then
    curl -Ls https://github.com/prometheus/prometheus/releases/download/v${version}/prometheus-${version}.linux-${architecture}.tar.gz | tar xzvf -
  fi

  mkdir -p ${etc_dir}
  mkdir -p ${bin_dir}
  mkdir -p ${prom_data_dir}

  systemctl stop prometheus

  cp ${unpack_dir}/{prometheus,promtool} ${bin_dir}
  cp prometheus-config.yml ${etc_dir}/prometheus.yml

  cp prometheus.service /lib/systemd/system/prometheus.service
  systemctl daemon-reload
  systemctl start prometheus
  systemctl enable prometheus
}

function install_node_exporter() {
  version=1.3.1
  architecture=$(get_architecture)
  unpack_dir=node_exporter-${version}.linux-${architecture}
  etc_dir=${ETC_DIR:-/etc/prometheus}
  bin_dir=${BIN_DIR:-/usr/local/bin}

  if ! [ -d ${unpack_dir} ]; then
    curl -Ls https://github.com/prometheus/node_exporter/releases/download/v${version}/node_exporter-${version}.linux-${architecture}.tar.gz | tar xzvf -
  fi

  mkdir -p ${bin_dir}

  systemctl stop node_exporter

  cp ${unpack_dir}/node_exporter ${bin_dir}

  cp node_exporter.service /lib/systemd/system/
  systemctl daemon-reload
  systemctl start node_exporter
  systemctl enable node_exporter

  # Add the scrape config and then restart prometheus
  add_node_exporter_scrape_config
  systemctl reload prometheus
}

function add_node_exporter_scrape_config() {
  prometheus_cfg=${PROMETHEUS_CONFIG:-/etc/prometheus/prometheus.yml}

  if grep 'job_name: node_exporter' ${prometheus_cfg} >/dev/null; then
    return
  fi
  echo "Adding node_exporter scrape config to ${prometheus_cfg}"
  cat <<EOF >> ${prometheus_cfg}

  - job_name: node_exporter
    static_configs:
      - targets: ['localhost:9100']
EOF
}

function install_alertmanager() {
  version=0.23.0
  architecture=$(get_architecture)
  unpack_dir=alertmanager-${version}.linux-${architecture}
  etc_dir=${ETC_DIR:-/etc/alertmanager}
  bin_dir=${BIN_DIR:-/usr/local/bin}
  prometheus_config=${PROMETHEUS_CONFIG:-/etc/prometheus/prometheus.yml}
  alertmanager_data_dir=${ALERTMANAGER_DATA_DIR:-/var/lib/alertmanager/data}
  if [[ -z "$SLACK_WEBHOOK_URL" ]]; then
	  echo "Missing \$SLACK_WEBHOOK_URL in environment. Get that from https://api.slack.com/apps" 
	  return
  fi

  if ! [ -d ${unpack_dir} ]; then
    curl -Ls https://github.com/prometheus/alertmanager/releases/download/v${version}/alertmanager-${version}.linux-${architecture}.tar.gz | tar xzvf -
  fi

  mkdir -p ${bin_dir}
  mkdir -p ${etc_dir}

  systemctl stop alertmanager

  cp ${unpack_dir}/alertmanager ${bin_dir}
  sed -e "s%SLACK_WEBHOOK_URL%${SLACK_WEBHOOK_URL}%" alertmanager-config.yml > ${etc_dir}/alertmanager.yml

  cp alertmanager.service /lib/systemd/system/
  systemctl daemon-reload
  systemctl start alertmanager
  systemctl enable alertmanager

  if ! grep 'alerting:' >/dev/null $prometheus_config; then
    cat <<EOF >> $prometheus_config

alerting:
  alertmanagers:
    - static_configs:
        - targets:
            - localhost:9093

rule_files:
  - "/etc/prometheus/alerts.yml"
EOF
  fi

  cat <<EOF > /etc/prometheus/alerts.yml
groups:
  - name: example
    rules:
      - alert: GarageDoorOpen
        expr: garagepy_door_state != 1
        for: 1h
        annotations:
          summary: 'Garage door has been open for over 1h'
EOF

  systemctl reload prometheus
}

function install() {
  INSTALL_PATH=$1

  apt install -y python3-gpiozero python3-flask python3-prometheus-client
  sed "s|GARAGEPYPATH|${INSTALL_PATH}/garagepy.sh|" ${INSTALL_PATH}/garagepy.service | tee /lib/systemd/system/garagepy.service
  systemctl daemon-reload
  systemctl restart garagepy
  systemctl enable garagepy
}
