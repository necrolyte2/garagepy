# garagepy

Garage Door Raspberry Pi monitoring

The aim of this project is to have a super simple system that monitors if your garage door(or any door really)
is open or closed.

You can read more about the details in the blog post about this project [here](http://tygertown.us/garagepy)

# Install 

The installer will install the garage.py script as a systemd service unit

```
./garagepy.sh install
```

# Monitoring Install

If you want prometheus installed as well as node\_exporter you can use the following installers that come with this project in the `functions.sh`

## Prometheus
```
. functions.sh
install_prometheus
```

## Node Exporter
```
. functions.sh
install_node_exporter
```

## Alertmanager
```
SLACK_WEBHOOK_URL=http://...
install_alertmanager
```
