#!/usr/bin/env python3

import gpiozero
import signal
import time
import flask
import prometheus_client

# Metrics stuff
CONTENT_TYPE_LATEST = str('text/plain; version=0.0.4; charset=utf-8')
door_metric_state = prometheus_client.Gauge('garagepy_door_state', 'Door state')

# Physical sensor stuff
DOOR_SENSOR_PIN = 18
## Mapping of GPIO activity
active_map = {'active': 'closed', 'inactive': 'open'}
door_sensor = gpiozero.DigitalInputDevice(DOOR_SENSOR_PIN, pull_up=True)
def active():
    print("Door {}".format(active_map['active']))
    door_metric_state.set(1)

def inactive():
    print("Door {}".format(active_map['inactive']))
    door_metric_state.set(0)

if door_sensor.active_time:
    active()
else:
    inactive()

door_sensor.when_activated = active
door_sensor.when_deactivated = inactive

# API stuff
app = flask.Flask(__name__)

@app.route("/")
def index():
    return 'Welcome to GaragePy.<br /> You May want to check out <a href="/state">Current State</a> or <a href="/metrics">Metrics</a>'

@app.route("/state")
def state():
    ret = {"state": "", "seconds_in_state": 0}
    if door_sensor.active_time:
        ret['state'] = active_map['active'] 
        ret['seconds_in_state'] = door_sensor.active_time
    else:
        ret['state'] = active_map['inactive'] 
        ret['seconds_in_state'] = door_sensor.inactive_time

    return flask.json.jsonify(ret)

@app.route('/metrics/')
def metrics():
    return flask.Response(prometheus_client.generate_latest(), mimetype=CONTENT_TYPE_LATEST)


if __name__ == '__main__':
    api()
